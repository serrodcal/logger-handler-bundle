# Logger Handler Bundle

[![pipeline status](https://gitlab.com/serrodcal/logger-handler-bundle/badges/master/pipeline.svg)](https://gitlab.com/serrodcal/logger-handler-bundle/commits/master)

This project aim to be an example for a Medium post. 

## Getting Started

### Prerequisites

Two ways:

* Have WSO2 ESB 5.0.0 instance in your local.
* By Docker.

### Installing

First, build JAR file:

```
~$ mvn package
```

Put generated JAR into `<ESB_HOME>/repository/components/dropins/`.

Configure `<ESB_HOME>/repository/conf/log4j.properties`:

```
log4j.rootLogger=ERROR, CARBON_CONSOLE, CARBON_LOGFILE, CARBON_MEMORY, CARBON_SYS_LOG, ERROR_LOGFILE, CUSTOM_LOGGER

...

log4j.logger.CUSTOM_LOGGER=INFO

...

# Appender config to send LoggerCustomHandler logs to new file.
log4j.appender.CUSTOM_LOGGER = org.apache.log4j.RollingFileAppender
log4j.appender.CUSTOM_LOGGER.layout = org.apache.log4j.PatternLayout
log4j.appender.CUSTOM_LOGGER.layout.ConversionPattern = %d{ISO8601} %-5p %c ~ %m%n
log4j.appender.CUSTOM_LOGGER.File = ${carbon.home}/repository/logs/custom_logger.log
log4j.appender.CUSTOM_LOGGER.Threshold = INFO
log4j.appender.CUSTOM_LOGGER.Append=true
```

Finally, configure `synapse-handlers.xml`:

```
<handlers>
    <handler name="LoggerHandler" class="com.serrodcal.esb.handler.LoggerHandler"/>
</handlers>
```

And then, run ESB.

### Testing

Test using OSGi Console (start ESB with `-DosgiConsole=19444`) and run `~$ telnet localhost 19444`:

```
osgi> lb logger-handler-bundle
START LEVEL 6
ID|State      |Level|Name
35|Active     |    4|logger-handler-bundle (1.0.0.SNAPSHOT)
```

### Docker

Use Docker from Registry to get this in your local.

```
~$ docker pull registry.gitlab.com/serrodcal/logger-handler-bundle:latest
~$ docker run -p9443:9443 -p8280:8280 -p19444:19444 <IMAGE_ID>
```

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [Apache Felix Maven Bundle Plugin](http://felix.apache.org/documentation/subprojects/apache-felix-maven-bundle-plugin-bnd.html) - OSGi Bundle

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Sergio Rodríguez Calvo** - *Development* - [Github](https://github.com/serrodcal)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
