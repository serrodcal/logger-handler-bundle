package com.serrodcal.esb.handler;

import com.google.common.collect.ImmutableSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.synapse.AbstractSynapseHandler;
import org.apache.synapse.MessageContext;

public class LoggerHandler extends AbstractSynapseHandler {

    private static final Log log = LogFactory.getLog("CUSTOM_LOGGER");

    @Override
    public boolean handleRequestInFlow(MessageContext messageContext) {

        this.log.info("handleRequestInFlow");

        ImmutableSet<String> COLOR_NAMES = ImmutableSet.of(
                "red",
                "orange",
                "yellow",
                "green",
                "blue",
                "purple");

        this.log.info(COLOR_NAMES.toString());

        return true;
    }

    @Override
    public boolean handleRequestOutFlow(MessageContext messageContext) {

        this.log.info("handleRequestOutFlow");

        return true;
    }

    @Override
    public boolean handleResponseInFlow(MessageContext messageContext) {

        this.log.info("handleResponseInFlow");

        return true;
    }

    @Override
    public boolean handleResponseOutFlow(MessageContext messageContext) {

        this.log.info("handleResponseOutFlow");

        return true;
    }
}
